document.addEventListener('wheel', function (event) {
    if (event.target.closest('.portfolio')) {
        // Allow normal scrolling inside the portfolio
        return;
    }

    event.preventDefault(); // Prevent the default scrolling behavior
    const container = document.querySelector('.container');
    const scrollAmount = window.innerWidth;
    let newScrollPosition = event.deltaY > 0 ? container.scrollLeft + scrollAmount : container.scrollLeft - scrollAmount;

    container.scrollTo({
        left: newScrollPosition,
        behavior: 'smooth' // Enable smooth scrolling
    });
}, { passive: false });

document.addEventListener('DOMContentLoaded', function () {
    var hoverLinks = document.querySelectorAll('.hover-link');
    var portfolioDiv = document.querySelector('.portfolio');
    var targetDivs = document.querySelectorAll('.skill-div'); // Adjust this selector to match your new divs
    var keepBoxVisible = false;

    hoverLinks.forEach(function (link) {
        var boxId = link.getAttribute('data-box');
        var box = document.getElementById(boxId);
        var children = box.children;

        link.addEventListener('mouseover', function () {
            box.style.display = 'grid'; // Ensure the box is displayed first
            setTimeout(function () {
                box.style.opacity = '1';
                box.style.visibility = 'visible';
                // Fade in each child element with a staggered effect
                Array.from(children).forEach((child, index) => {
                    setTimeout(() => {
                        child.style.opacity = 1;
                    }, index * 100); // Staggered delay, adjust time as needed
                });
            }, 0); // Timeout to allow for display: block to apply first

            if (link.textContent.includes('humans')) {
                keepBoxVisible = true;
            } else {
                keepBoxVisible = false;
            }
        });

        link.addEventListener('mouseout', function () {
            if (!keepBoxVisible) {
                box.style.opacity = '0';
                setTimeout(function () {
                    box.style.visibility = 'hidden';
                    box.style.display = 'none'; // Hide the box after opacity transition
                }, 500); // Timeout corresponds to the transition duration
            }
        });
    });

    // Event listener for portfolio links
    document.querySelectorAll('.portfolio-link').forEach(function (link) {
        link.addEventListener('click', function (e) {
            e.preventDefault();
            var fileToLoad = link.getAttribute('data-file');

            fetch(fileToLoad)
                .then(response => response.text())
                .then(html => {
                    var portfolioDiv = document.querySelector('.portfolio');
                    portfolioDiv.innerHTML = html;
                    portfolioDiv.style.display = 'block';

                    setTimeout(function () {
                        portfolioDiv.style.opacity = '1';
                        portfolioDiv.style.visibility = 'visible';
                    }, 0);

                    // Attach event listener to the close button after content is loaded
                    var closeButton = portfolioDiv.querySelector('.portfolio-close');
                    closeButton.addEventListener('click', function () {
                        portfolioDiv.style.opacity = '0';
                        setTimeout(function () {
                            portfolioDiv.style.display = 'none';
                        }, 500); // Assuming the opacity transition is 500ms
                    });
                })
                .catch(error => {
                    console.error('Error loading portfolio content:', error);
                });
        });
    });









});
//NAV
document.addEventListener('DOMContentLoaded', function () {
    var selectedWorksLink = document.getElementById('selected-works-link');
    var contactLinks = document.getElementsByClassName('contact-link');
    var forHumansSection = document.getElementById('for-humans-section');
    var contactCard = document.getElementById('contact-card');
    var closeContactCard = document.getElementById('close-contact-card');

    selectedWorksLink.addEventListener('click', function (e) {
        e.preventDefault();
        forHumansSection.scrollIntoView({ behavior: 'smooth' });
    });

    Array.from(contactLinks).forEach(function (contactLink) {
        contactLink.addEventListener('click', function (e) {
            e.preventDefault();
            contactCard.style.display = 'block';
        });
    });

    closeContactCard.addEventListener('click', function () {
        contactCard.style.display = 'none';
    });
});