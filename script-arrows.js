// script-arrows.js

document.addEventListener('DOMContentLoaded', function () {
    const sections = document.querySelectorAll('.section');
    let currentSectionIndex = 0;

    const leftArrow = document.createElement('div');
    leftArrow.classList.add('arrow', 'arrow-left');
    leftArrow.innerHTML = '&#8592;'; // Left arrow symbol
    document.body.appendChild(leftArrow);

    const rightArrow = document.createElement('div');
    rightArrow.classList.add('arrow', 'arrow-right');
    rightArrow.innerHTML = '&#8594;'; // Right arrow symbol
    document.body.appendChild(rightArrow);

    const updateArrows = () => {
        leftArrow.style.display = currentSectionIndex === 0 ? 'none' : 'block';
        rightArrow.style.display = currentSectionIndex === sections.length - 1 ? 'none' : 'block';
    };

    const scrollToSection = (index) => {
        sections[index].scrollIntoView({ behavior: 'smooth' });
        currentSectionIndex = index;
        updateArrows();
    };

    leftArrow.addEventListener('click', () => {
        if (currentSectionIndex > 0) {
            scrollToSection(currentSectionIndex - 1);
        }
    });

    rightArrow.addEventListener('click', () => {
        if (currentSectionIndex < sections.length - 1) {
            scrollToSection(currentSectionIndex + 1);
        }
    });

    // Function to determine the closest section index based on scroll position
    const findClosestSectionIndex = () => {
        let closestIndex = 0;
        let minDistance = Number.MAX_VALUE;
        sections.forEach((section, index) => {
            const distance = Math.abs(section.getBoundingClientRect().top);
            if (distance < minDistance) {
                minDistance = distance;
                closestIndex = index;
            }
        });
        return closestIndex;
    };

    // Update the current section index and arrow visibility on scroll
    window.addEventListener('wheel', () => {
        currentSectionIndex = findClosestSectionIndex();
        updateArrows();
    });

    updateArrows(); // Initial update for arrow visibility
});
