//fade up anims
document.addEventListener('DOMContentLoaded', function () {
    var elements = document.querySelectorAll('.animate-on-scroll');

    function isElementInViewport(el) {
        var rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    function checkVisibility() {
        for (var i = 0; i < elements.length; i++) {
            if (isElementInViewport(elements[i])) {
                elements[i].classList.add('fadeInUp');
            }
        }
    }

    // Check if elements are visible on load
    checkVisibility();

    // Check if elements are visible on scroll
    window.addEventListener('scroll', checkVisibility);
});
